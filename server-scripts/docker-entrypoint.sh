#!/bin/bash
set -e

cd /home/mcserver
if [[ -f ServerStart.sh ]]; then
  exec su mcserver ServerStart.sh "$@"
elif [[ -f /alt-ServerStart.sh ]]; then
  cp /alt-ServerStart.sh alt-ServerStart.sh
  cp /alt-settings.cfg alt-settings.cfg
  exec su mcserver alt-ServerStart.sh "$@"
fi

exec "$@"
